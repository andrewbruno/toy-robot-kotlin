# Toy Robot Challenge

Toy Robot Code Challenge implementation written in Kotlin (1.3.10) as per [PROBLEM](PROBLEM.md) requirements.

## Dependencies

Java 1.8+ and internet access to download kotlin dependencies via Gradle build.   

## Quick Start

```
git clone https://bitbucket.org/andrewbruno/toy-robot-kotlin.git
./gradlew build && java -jar build/libs/toyrobot.jar
```

## Build & Test

`./gradlew clean build test`

## Extra Feature

`Q` command exits the program.

You can pass in a file of commands with the last command being a `Q` so that the program gracefully exits.

## Run via Console

`java -jar build/libs/toyrobot.jar`

## Run via file input

```
java -jar build/libs/toyrobot.jar < build/resources/test/example-a.txt
java -jar build/libs/toyrobot.jar < build/resources/test/example-b.txt
java -jar build/libs/toyrobot.jar < build/resources/test/example-c.txt
java -jar build/libs/toyrobot.jar < build/resources/test/example-snake-columns.txt 
java -jar build/libs/toyrobot.jar < build/resources/test/example-snake-rows.txt 
```

## Assumptions

Commands are taken as they are typed.  They are not trimmed, nor upper cased, etc.
If a command is invalid, it is simply ignored.
The only exception is for when REPORT is called on an un-PLACED robot, the user is notified.
