package toyrobot.controller

import toyrobot.domain.Robot
import toyrobot.view.OutputHandler

data class Table(
        val size: Int = 5,
        val console: OutputHandler) : Grid {
    private var robot: Robot? = null

    private fun canMove(x: Int, y: Int): Boolean {
        return (x in 0..(size - 1)) && (y in 0..(size - 1))
    }

    private fun canMove(xyPair: Pair<Int, Int>): Boolean {
        return canMove(xyPair.first, xyPair.second)
    }

    override fun place(x: Int, y: Int, d: Direction) {
        if (canMove(x, y)) {
            if (robot == null) {
                robot = Robot(x, y, d)
            } else {
                robot!!.setXYD(x,y,d)
            }
        }
    }

    override fun move() {
        robot?.let {
            val xy = it.d.next(it.x, it.y)
            if (canMove(xy)) {
                it.setXY(xy)
            }
        }
    }

    override fun left() {
        robot?.let { it.d = it.d.rotateLeft() }
    }

    override fun right() {
        robot?.let { it.d = it.d.rotateRight() }
    }

    override fun report() {
        if (robot != null) {
            val x = robot!!.x.toString()
            val y = robot!!.y.toString()
            val d = robot!!.d.name
            console.display("Output: $x,$y,$d")
        } else {
            console.display("Output: Robot has not been placed")
        }
    }
}
