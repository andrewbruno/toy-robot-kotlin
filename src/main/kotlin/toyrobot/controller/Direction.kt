package toyrobot.controller

enum class Direction(
        val rotateLeft: () -> Direction,
        val rotateRight: () -> Direction,
        val next: (x: Int, y: Int) -> Pair<Int, Int>
) {
    NORTH({ WEST }, { EAST }, { x, y -> Pair(x, y + 1) }),
    EAST({ NORTH }, { SOUTH }, { x, y -> Pair(x + 1, y) }),
    WEST({ SOUTH }, { NORTH }, { x, y -> Pair(x - 1, y) }),
    SOUTH({ EAST }, { WEST }, { x, y -> Pair(x, y - 1) })
}
