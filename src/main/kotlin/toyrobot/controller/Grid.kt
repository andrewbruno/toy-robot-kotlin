package toyrobot.controller

interface Grid {
    fun place(x: Int, y: Int, d: Direction)
    fun move()
    fun report()
    fun left()
    fun right()
}