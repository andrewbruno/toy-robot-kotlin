package toyrobot.controller

data class Command(val grid: Grid) {

    enum class CMD {
        PLACE,
        MOVE,
        LEFT,
        RIGHT,
        REPORT
    }

    fun execute(rawCommand: String) {
        val args = rawCommand.split(" ").map { it }

        val command = CMD.valueOf(args.first())

        when (command) {
            CMD.PLACE -> {
                val instructions = args[1].split(",").map { it }
                val x = instructions[0].toInt()
                val y = instructions[1].toInt()
                val d = instructions[2]
                grid.place(x, y, Direction.valueOf(d))
            }
            CMD.MOVE -> grid.move()
            CMD.LEFT -> grid.left()
            CMD.RIGHT -> grid.right()
            CMD.REPORT -> grid.report()
        }
    }
}