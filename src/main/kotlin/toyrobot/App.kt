package toyrobot

import toyrobot.controller.Command
import toyrobot.controller.Table
import toyrobot.view.Console
import toyrobot.view.VerifyInput

class App {
    val greeting: String
        get() {
            return "\n-- Toy Robot Simulator -- \n\nPlease enter your commands:\n"
        }
}

fun main(args: Array<String>) {
    val console = Console()
    console.display(App().greeting)

    val command = Command(Table(5, console))

    do {
        print("> ")
        val input = readLine()!!
        if (VerifyInput.isCommandValid(input)) {
            command.execute(input)
        }
    } while (input.toUpperCase() != "Q")
}
