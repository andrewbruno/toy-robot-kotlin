package toyrobot.view

class VerifyInput {
    companion object {
        private const val CMD_PLACE = "PLACE [0-4],[0-4],(NORTH|EAST|SOUTH|WEST)"
        private const val CMD_OTHER = "MOVE|LEFT|RIGHT|REPORT"
        private const val REGEX_CMD = "($CMD_PLACE)|($CMD_OTHER)"

        fun isCommandValid(userInput: String): Boolean {
            return REGEX_CMD.toRegex().matches(userInput)
        }
    }
}