package toyrobot.view

interface OutputHandler {
    fun display(message: String)
}

class Console : OutputHandler {
    override fun display(message: String) {
        println(message)
    }
}