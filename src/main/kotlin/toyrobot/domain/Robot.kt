package toyrobot.domain

import toyrobot.controller.Direction

data class Robot(var x: Int, var y: Int, var d: Direction) {

    fun setXY(xyPair: Pair<Int, Int>) {
        this.x = xyPair.first
        this.y = xyPair.second
    }

    fun setXYD(x: Int, y: Int, d: Direction) {
        this.x = x
        this.y = y
        this.d = d
    }
}