package toyrobot

import toyrobot.controller.Command
import toyrobot.controller.Table
import toyrobot.view.OutputHandler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class E2ETest {

    class ConsoleMock: OutputHandler {
        var lastDisplayMessage = ""
        override fun display(message: String) {
            lastDisplayMessage = message
        }

        fun reset() {
            lastDisplayMessage = ""
        }
    }

    private var console = ConsoleMock()
    private val table = Table(5, console)
    private val c = Command(table)

    @BeforeTest
    fun setUp() {
        console.reset()
    }

    @Test
    fun `should run example A`() {
        c.execute("PLACE 0,0,NORTH")
        c.execute("MOVE")
        c.execute("REPORT")
        assertEquals("Output: 0,1,NORTH", console.lastDisplayMessage)
    }

    @Test
    fun `should run example B`() {
        c.execute("PLACE 0,0,NORTH")
        c.execute("LEFT")
        c.execute("REPORT")
        assertEquals("Output: 0,0,WEST", console.lastDisplayMessage)
    }

    @Test
    fun `should run example C`() {
        c.execute("PLACE 1,2,EAST")
        c.execute("MOVE")
        c.execute("MOVE")
        c.execute("LEFT")
        c.execute("MOVE")
        c.execute("REPORT")
        assertEquals("Output: 3,3,NORTH", console.lastDisplayMessage)
    }

    @Test
    fun `should run example D, commands before PLACE, and after PLACE`() {
        c.execute("MOVE")
        c.execute("LEFT")
        c.execute("RIGHT")

        c.execute("REPORT")
        assertEquals("Output: Robot has not been placed", console.lastDisplayMessage)

        c.execute("PLACE 1,2,EAST")
        c.execute("MOVE")
        c.execute("MOVE")
        c.execute("LEFT")
        c.execute("RIGHT")
        c.execute("MOVE")

        c.execute("REPORT")
        assertEquals("Output: 4,2,EAST", console.lastDisplayMessage)

        c.execute("PLACE 0,0,SOUTH")

        c.execute("REPORT")
        assertEquals("Output: 0,0,SOUTH", console.lastDisplayMessage)
    }
}