package toyrobot.controller

import kotlin.test.Test
import kotlin.test.assertEquals

class ControllerTest {
    data class TableMock(val size: Int = 5) : Grid {
        var lastActionCalled = ""

        override fun place(x: Int, y: Int, d: Direction) {
            lastActionCalled = "place"
        }

        override fun left() {
            lastActionCalled = "left"
        }

        override fun right() {
            lastActionCalled = "right"
        }

        override fun move() {
            lastActionCalled = "move"
        }

        override fun report() {
            lastActionCalled = "report"
        }
    }

    @Test
    fun `should fire off valid commands`() {
        val table = TableMock(0)
        val command = Command(table)

        command.execute("PLACE 1,1,EAST")
        assertEquals("place", table.lastActionCalled)

        command.execute("MOVE")
        assertEquals("move", table.lastActionCalled)

        command.execute("LEFT")
        assertEquals("left", table.lastActionCalled)

        command.execute("RIGHT")
        assertEquals("right", table.lastActionCalled)

        command.execute("REPORT")
        assertEquals("report", table.lastActionCalled)
    }
}