package toyrobot.controller

import toyrobot.view.OutputHandler
import kotlin.test.Test
import kotlin.test.assertEquals

class TableTest {

    private class MockConsole : OutputHandler {
        private var lastMessageDisplayed = ""

        override fun display(message: String) {
            lastMessageDisplayed = message
        }

        fun getLastMessageDispalyed(): String {
            return lastMessageDisplayed
        }
    }

    private val mockConsole = MockConsole()
    private val table = Table(5, mockConsole)

    @Test
    fun `should not move when not placed`() {
        table.move()
        table.report()
        assertEquals("Output: Robot has not been placed", mockConsole.getLastMessageDispalyed())
    }

    @Test
    fun `should not rotateLeft when not placed`() {
        table.left()
        table.report()
        assertEquals("Output: Robot has not been placed", mockConsole.getLastMessageDispalyed())
    }

    @Test
    fun `should not rotateRight when not placed`() {
        table.right()
        table.report()
        assertEquals("Output: Robot has not been placed", mockConsole.getLastMessageDispalyed())
    }

    @Test
    fun `should not place when not placed inside table`() {
        table.place(5, 5, Direction.NORTH)
        table.report()
        assertEquals("Output: Robot has not been placed", mockConsole.getLastMessageDispalyed())
    }

    @Test
    fun `should place when placed inside table`() {
        for (x in 0..4) {
            for (y in 0..4) {
                table.place(x, y, Direction.NORTH)
                table.report()
                assertEquals("Output: $x,$y,NORTH", mockConsole.getLastMessageDispalyed())
            }
        }
    }

    @Test
    fun `should move when space allows`() {
        table.place(1, 1, Direction.EAST)
        table.move()
        table.report()
        assertEquals("Output: 2,1,EAST", mockConsole.getLastMessageDispalyed())
    }

    @Test
    fun `should not move when space not allowed`() {
        table.place(4, 4, Direction.EAST)
        table.move()
        table.report()
        assertEquals("Output: 4,4,EAST", mockConsole.getLastMessageDispalyed())
    }
}