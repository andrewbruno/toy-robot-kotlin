package toyrobot.controller

import kotlin.test.Test
import kotlin.test.assertEquals

class DirectionTest {
    @Test
    fun `verify rotations`() {
        assertEquals(Direction.WEST, Direction.NORTH.rotateLeft())
        assertEquals(Direction.EAST, Direction.NORTH.rotateRight())

        assertEquals(Direction.NORTH, Direction.EAST.rotateLeft())
        assertEquals(Direction.SOUTH, Direction.EAST.rotateRight())

        assertEquals(Direction.EAST, Direction.SOUTH.rotateLeft())
        assertEquals(Direction.WEST, Direction.SOUTH.rotateRight())

        assertEquals(Direction.SOUTH, Direction.WEST.rotateLeft())
        assertEquals(Direction.NORTH, Direction.WEST.rotateRight())
    }

    @Test
    fun `verify next position`() {
        assertEquals(Pair(2, 3), Direction.NORTH.next(2, 2))
        assertEquals(Pair(3, 2), Direction.EAST.next(2, 2))
        assertEquals(Pair(2, 1), Direction.SOUTH.next(2, 2))
        assertEquals(Pair(1, 2), Direction.WEST.next(2, 2))
    }
}