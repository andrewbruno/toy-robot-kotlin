package toyrobot.view

import kotlin.test.Test
import kotlin.test.assertFalse

class VerifyInputTest {

    @Test
    fun `verify place commands`() {
        assert(VerifyInput.isCommandValid("PLACE 0,0,NORTH"))
        assert(VerifyInput.isCommandValid("PLACE 2,2,SOUTH"))
        assert(VerifyInput.isCommandValid("PLACE 3,3,EAST"))
        assert(VerifyInput.isCommandValid("PLACE 4,4,WEST"))
    }

    @Test
    fun `verify single word commands`() {
        assert(VerifyInput.isCommandValid("MOVE"))
        assert(VerifyInput.isCommandValid("REPORT"))
        assert(VerifyInput.isCommandValid("LEFT"))
        assert(VerifyInput.isCommandValid("RIGHT"))
        assert(VerifyInput.isCommandValid("REPORT"))
    }

    @Test
    fun `verify invalid single word commands`() {
        assertFalse(VerifyInput.isCommandValid("MOVEr"))
        assertFalse(VerifyInput.isCommandValid(" REPORT"))
        assertFalse(VerifyInput.isCommandValid("LEFT "))
        assertFalse(VerifyInput.isCommandValid(" RIGHT"))
    }

    @Test
    fun `verify invalid place word commands`() {
        assertFalse(VerifyInput.isCommandValid("PLACE NORTH,0,0"))
        assertFalse(VerifyInput.isCommandValid("PLACE 5,5,EAST"))
        assertFalse(VerifyInput.isCommandValid("PLACE 0,5,WEST"))
        assertFalse(VerifyInput.isCommandValid(" PLACE 1,2,SOUTH"))
    }
}